package cn.uncode.baas.server.internal.module.data;

import java.util.List;
import java.util.Map;

import cn.uncode.baas.server.exception.ValidateException;

public interface IDataModule {
	
	List<Map<String, Object>> find(Object param) throws ValidateException;
	
	Map<String, Object> findOne(Object param) throws ValidateException;
	
	int update(Object param) throws ValidateException;
	
	int remove(Object param) throws ValidateException;
	
	Object insert(Object param) throws ValidateException;
	
	int count(Object param) throws ValidateException;
	
	
}
